#include <string.h>
#include <ncurses.h>

#include "draw.h"
#include "game.h"

/* ncurses windows / box / wborder don't work, no idea why. I
 * implemented my own window handling
 * struct window encodes absolute windows but using the abstracted
 * coordinate system (x is half as wide)
 */
struct window {
    int x;
    int y;
    int width;
    int height;
};

/* struct window_real encodes raw absolute screen positions extracted
 * from struct window
 */
struct window_real {
    int left;
    int right;
    int top;
    int bottom;
};

static struct window windows[DRAW_WINDOW_END] = {
    [DRAW_WINDOW_GAMEFIELD] = {
        1,
        1,
        FIELD_WIDTH + 1,
        FIELD_HEIGHT + 1,
    },

    [DRAW_WINDOW_STATUSBAR] = {
        1                      - 1,
        1 + FIELD_HEIGHT + 1      ,
        FIELD_WIDTH + 1        + 2,
        5                         ,
    },
};

static inline struct window_real  get_window_real  (struct window *win);

static inline struct window_real
get_window_real(struct window *win)
{
    struct window_real ret;

    ret.left = win->x * 2;
    ret.top = win->y;
    ret.right = ret.left + win->width * 2;
    ret.bottom = ret.top + win->height;

    return ret;
}

void
draw_single_border(int window)
{
    struct window_real win = get_window_real(windows + window);

    mvvline(win.top,    win.left,  0, win.bottom - win.top);
    mvvline(win.top,    win.right, 0, win.bottom - win.top);
    mvhline(win.top,    win.left,  0, win.right - win.left);
    mvhline(win.bottom, win.left,  0, win.right - win.left);
    mvaddch(win.top,    win.left,  ACS_ULCORNER);
    mvaddch(win.top,    win.right, ACS_URCORNER);
    mvaddch(win.bottom, win.left,  ACS_LLCORNER);
    mvaddch(win.bottom, win.right, ACS_LRCORNER);
}

void
draw_window_clear(int window)
{
    struct window_real win = get_window_real(windows + window);

    for (int y = win.top + 1; y < win.bottom; ++y)
        for (int x = win.left + 1; x < win.right; ++x)
            mvaddch(y, x, ' ');
}

/* Attempts to join borders of windows. They must have their edges
 * touching or else the function will return 0
 *
 * Currently there is only the two windows, so I only bothered to
 * implement the case where w2 is touching w1 from below
 */
int
draw_join_windows(int window1, int window2)
{
    struct window_real w1 = get_window_real(windows + window1);
    struct window_real w2 = get_window_real(windows + window2);

    if (w2.top == w1.bottom) {
        if (w2.left == w1.left)
            mvaddch(w2.top, w2.left, ACS_LTEE);
        else if (w2.left > w1.left)
            mvaddch(w2.top, w2.left, ACS_TTEE);
        else
            mvaddch(w1.bottom, w1.left, ACS_BTEE);

        if (w2.right == w1.right)
            mvaddch(w2.top, w2.right, ACS_RTEE);
        else if (w2.right < w1.right)
            mvaddch(w2.top, w2.right, ACS_TTEE);
        else
            mvaddch(w1.bottom, w1.right, ACS_BTEE);

        return 1;
    }

    return 0;
}

void
draw_str_at(char *str, int window, int x, int y)
{
    struct window_real win = get_window_real(windows + window);

    mvaddstr(win.top + 1 + y, win.left + 2 +  x * 2, str);
}

void
draw_ch_at(char ch, int window, int x, int y)
{
    struct window_real win = get_window_real(windows + window);

    mvaddch(win.top + 1 + y, win.left + 2 + x * 2, ch);
}

void
draw_borders(void)
{
    draw_single_border(DRAW_WINDOW_GAMEFIELD);
    draw_single_border(DRAW_WINDOW_STATUSBAR);
    draw_join_windows(DRAW_WINDOW_GAMEFIELD, DRAW_WINDOW_STATUSBAR);
}

void
draw_end(void)
{
    curs_set(1);
    endwin();
}

void
draw_init(void)
{
    initscr();
    noecho();
    curs_set(0);
}

void
draw_refresh(void)
{
    refresh();
}

void
draw_score(int score, size_t snakelength)
{
    static char buffer[64];

    sprintf(buffer, "Score:  %u", score);
    draw_str_at(buffer, DRAW_WINDOW_STATUSBAR, 0, 2);

    sprintf(buffer, "Length: %lu", snakelength);
    draw_str_at(buffer, DRAW_WINDOW_STATUSBAR, 0, 3);
}

void
draw_time(int ticks)
{
    static char floatbuffer[32];
    static char buffer[64];
    float time_elapsed = (float)ticks / FPS;

    sprintf(floatbuffer, "%.2fs", time_elapsed);
    sprintf(buffer, "Time:   %-6s (%u ticks)", floatbuffer, ticks);
    draw_str_at(buffer, DRAW_WINDOW_STATUSBAR, 0, 1);
}

void
draw_game_mode(char const *game_mode)
{
    static char buffer[64];

    sprintf(buffer, "Mode:   %s", game_mode);
    draw_str_at(buffer, DRAW_WINDOW_STATUSBAR, 0, 0);
}
