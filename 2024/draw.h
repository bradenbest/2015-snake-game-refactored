#ifndef DRAW_H
#define DRAW_H

enum draw_window {
    DRAW_WINDOW_GAMEFIELD,
    DRAW_WINDOW_STATUSBAR,
    DRAW_WINDOW_END,
};

void  draw_single_border  (int window);
void  draw_window_clear   (int window);
int   draw_join_windows   (int window1, int window2);
void  draw_str_at         (char *str, int window, int x, int y);
void  draw_ch_at          (char ch, int window, int x, int y);
void  draw_borders        (void);
void  draw_end            (void);
void  draw_init           (void);
void  draw_refresh        (void);
void  draw_score          (int score, size_t snakelength);
void  draw_time           (int ticks);
void  draw_game_mode      (char const *game_mode);

#endif // DRAW_H
