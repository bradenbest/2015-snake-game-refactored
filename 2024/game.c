#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "game.h"
#include "snake.h"
#include "draw.h"

static clock_t const TICK_INTERVAL = CLOCKS_PER_SEC / FPS;

static char const *gametype_str[] = {
    "Hit = Score -10; Score < 0 = Game Over",
    "Hit = Game Over",
};

static clock_t last_tick;
static int     elapsed_ticks;
static int     foodposition1d;
static int     game_score;

static inline void  game_loop_draw_food  (void);
static inline int   game_loop_step       (clock_t timestamp);

int
game_food_at(int x, int y)
{
    return y * FIELD_WIDTH + x == foodposition1d;
}

int
game_change_score(int change)
{
    game_score += change;

    return game_score >= 0;
}

/* 1. Rendering optimization: clear char at old food position before
 *    repositioning it
 */
void
game_field_insert_food(void)
{
    int food_x = foodposition1d % FIELD_WIDTH;
    int food_y = foodposition1d / FIELD_WIDTH;

    draw_ch_at(' ', DRAW_WINDOW_GAMEFIELD, food_x, food_y); // 1
    foodposition1d = rand() % (FIELD_WIDTH * FIELD_HEIGHT);
}

static inline void
game_loop_draw_food(void)
{
    int food_x = foodposition1d % FIELD_WIDTH;
    int food_y = foodposition1d / FIELD_WIDTH;

    draw_ch_at('+', DRAW_WINDOW_GAMEFIELD, food_x, food_y);
}

/* 1, Rendering optimization: Game Field is only cleared on every 100th
 *    tick. I could get away with not calling it at all, but there are
 *    still minor rendering bugs that result in e.g. a food artifact
 *    being left over after the food has been moved.
 */
static inline int
game_loop_step(clock_t timestamp)
{
    clock_t interval = timestamp - last_tick;

    if (interval < TICK_INTERVAL)
        return 1;

    last_tick = timestamp;
    ++elapsed_ticks;

    if (elapsed_ticks % 1000 == 0) {
        game_field_insert_food();
        game_score -= 20;
    }

    if (elapsed_ticks % 100 == 0) // 1
        draw_window_clear(DRAW_WINDOW_GAMEFIELD);

    if (!snake_update())
        return 0;

    game_loop_draw_food();
    draw_window_clear(DRAW_WINDOW_STATUSBAR);
    draw_game_mode(gametype_str[GAME_TYPE]);
    draw_score(game_score, snake_get_length());
    draw_time(elapsed_ticks);
    draw_refresh();
    return 1;
}

int
main(void)
{
    srand(time(NULL));
    snake_init(FIELD_WIDTH / 2, FIELD_HEIGHT / 2, SNAKE_SIZE);
    draw_init();

    last_tick = clock();
    game_field_insert_food();
    draw_borders();

    while (game_loop_step(clock()))
        ;

    draw_end();
    printf("Game Over.\n");

    if (GAME_TYPE == GAME_KILL_PLAYER)
        printf("Score: %u\n", game_score);
    else
        printf("Score: 0\n");

    return 0;
}
