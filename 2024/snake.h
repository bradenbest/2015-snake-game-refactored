#ifndef SNAKE_H
#define SNAKE_H

void    snake_init        (int x, int y, int snake_size);
int     snake_update      (void);
size_t  snake_get_length  (void);

#endif // SNAKE_H
