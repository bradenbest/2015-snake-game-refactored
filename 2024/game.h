#ifndef GAME_H
#define GAME_H

#ifndef FPS
#    define FPS 15
#endif

#ifndef GAME_TYPE
#    define GAME_TYPE GAME_KILL_PLAYER
#endif

#ifndef FIELD_WIDTH
#    define FIELD_WIDTH 30
#endif

#ifndef FIELD_HEIGHT
#    define FIELD_HEIGHT 30
#endif

#ifndef SNAKE_SIZE
#    define SNAKE_SIZE 10
#endif

enum gametype {
    GAME_REDUCE_SCORE,
    GAME_KILL_PLAYER,
};

int   game_food_at            (int x, int y);
int   game_change_score       (int change);
void  game_field_insert_food  (void);

#endif // GAME_H
