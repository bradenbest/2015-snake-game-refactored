#include <ncurses.h>

#include "game.h"
#include "snake.h"
#include "draw.h"

struct coord {
    int x;
    int y;
    int flags;
};

enum snake_flag {
    SNAKE_NONE        = 0x0,
    SNAKE_INITIALIZED = 0x1,
};

static struct coord position;
static struct coord direction;
static struct coord snake_pieces[FIELD_WIDTH * FIELD_HEIGHT];
static size_t snake_pieces_len;
static size_t snake_pieces_off;

static inline int   snake_do_input         (void);
static inline void  set_coord              (struct coord *coord, int x, int y);
static inline void  snake_move             (void);
static inline void  snake_update_pieces    (void);
static inline int   snake_check_collision  (void);
static inline void  draw_snake             (void);

static int  key_callback_dir_horiz  (struct coord *coord);
static int  key_callback_dir_vert   (struct coord *coord);
static int  key_callback_pause      (struct coord *unused);
static int  key_callback_quit       (struct coord *unused);

void
snake_init(int x, int y, int snake_size)
{
    set_coord(&position, x, y);
    set_coord(&direction, 0, -1);
    snake_pieces_len = snake_size;
}

/* 1. By moving the collision check to before the snake's head position
 *    is added to the pieces array, the special case in the collision
 *    check can be removed
 */
int
snake_update(void)
{
    if (!snake_do_input())
        return 0;

    snake_move();

    if (!snake_check_collision()) // 1
        return 0;

    snake_update_pieces();
    draw_snake();

    return 1;
}

size_t
snake_get_length(void)
{
    return snake_pieces_len;
}

static inline void
set_coord(struct coord *coord, int x, int y)
{
    coord->x = x;
    coord->y = y;
    coord->flags |= SNAKE_INITIALIZED;
}

static int
key_callback_dir_horiz(struct coord *coord)
{
    if (direction.x != -(coord->x))
        set_coord(&direction, coord->x, coord->y);

    return 1;
}

static int
key_callback_dir_vert(struct coord *coord)
{
    if (direction.y != -(coord->y))
        set_coord(&direction, coord->x, coord->y);

    return 1;
}

static int
key_callback_pause(struct coord *unused)
{
    (void) unused;

    draw_window_clear(DRAW_WINDOW_STATUSBAR);
    draw_str_at("Status: Paused", DRAW_WINDOW_STATUSBAR, 0, 0);

    while (getch() != 'p')
        ;

    return 1;
}

static int
key_callback_quit(struct coord *unused)
{
    (void) unused;
    return 0;
}

static inline int
snake_do_input(void)
{
    int ch;

    static struct {
        char keybind;
        int (*callback)(struct coord *);
        struct coord new_coord;
    } actions[] = {
        {'q', key_callback_quit,      { 0,  0, SNAKE_NONE}},
        {'p', key_callback_pause,     { 0,  0, SNAKE_NONE}},
        {'a', key_callback_dir_horiz, {-1,  0, SNAKE_NONE}},
        {'d', key_callback_dir_horiz, {+1,  0, SNAKE_NONE}},
        {'w', key_callback_dir_vert,  { 0, -1, SNAKE_NONE}},
        {'s', key_callback_dir_vert,  { 0, +1, SNAKE_NONE}},
    };

    timeout(0);
    ch = getch();

    for (size_t i = 0; i < sizeof actions / sizeof *actions; ++i)
        if (ch == actions[i].keybind)
            return actions[i].callback(&(actions[i].new_coord));

    return 1;
}

static inline void
snake_move(void)
{
    position.x = (FIELD_WIDTH + position.x + direction.x) % FIELD_WIDTH;
    position.y = (FIELD_HEIGHT + position.y + direction.y) % FIELD_HEIGHT;
}

/* 1. Rendering optimization. Rather than clear the whole window, just
 *    clear the one tail piece that actually needs to be cleared
 */
static inline void
snake_update_pieces(void)
{
    struct coord *selpiece = snake_pieces + snake_pieces_off;

    draw_ch_at(' ', DRAW_WINDOW_GAMEFIELD, selpiece->x, selpiece->y); // 1
    set_coord(selpiece, position.x, position.y);
    snake_pieces_off = (snake_pieces_off + 1) % snake_pieces_len;
}

/* 1. If the flag SNAKE_INITIALIZED is not set, the piece is
 *    uninitialized and shouldn't be considered in collisions and drawing
 *
 * 2. original implementation pushed to the left. This is pushing to the
 *    right
 *
 * 3. Penalizes the player by subtracting 50 points and if the snake
 *    piece happened to overlap the food, the snake also "misses" it.
 */
static inline int
snake_check_collision(void)
{
    for (size_t i = 0; i < snake_pieces_len; ++i) {
        struct coord *selpiece = snake_pieces + i;

        if (!(selpiece->flags & SNAKE_INITIALIZED))
            continue; // 1

        if (position.x == selpiece->x && position.y == selpiece->y)
            goto exit_self_collision;
    }

    if (game_food_at(position.x, position.y)) {
        game_field_insert_food();
        snake_pieces_len += 5; // 2
        game_change_score(+10);
    }

    return 1;

exit_self_collision:
    switch (GAME_TYPE) {
        case GAME_REDUCE_SCORE:
            return game_change_score(-50); // 3

        case GAME_KILL_PLAYER:
            return 0;

        default:
            return 0;
    }
}

/* 1. Don't draw uninitialized snake pieces
 *
 * 2. Don't draw over the food
 */
static inline void
draw_snake(void)
{
    for (size_t i = 0; i < snake_pieces_len; ++i) {
        struct coord *selpiece = snake_pieces + i;

        if (!(selpiece->flags & SNAKE_INITIALIZED))
            continue; // 1

        if (selpiece->x == position.x && selpiece->y == position.y)
            draw_ch_at('o', DRAW_WINDOW_GAMEFIELD, selpiece->x, selpiece->y);
        else if (game_food_at(selpiece->x, selpiece->y))
            continue; // 2
        else
            draw_ch_at('.', DRAW_WINDOW_GAMEFIELD, selpiece->x, selpiece->y);
    }
}
