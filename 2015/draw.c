#include <ncurses.h>

#include "draw.h"
#include "game.h"
#include "snake.h"


static void draw_border(struct game_field *gf);

static void
draw_border(struct game_field *gf)
    /* Ho-lee crap, this is the worst thing ever. */
{/*{{{*/
    int right  = gf->width * 2 + PADDING_X,
        bottom = gf->height + PADDING_Y + 2,
        left   = 2,
        top    = 1,
        vert   = bottom,
        horiz  = right - 1;

    /* Main border */
    mvvline(top,    left,  0, vert);  /* Left */
    mvvline(top,    right, 0, vert);  /* Right*/
    mvhline(top,    left,  0, horiz); /* Top */
    mvhline(bottom, left,  0, horiz); /* Bottom */
    mvaddch(top,    left,  ACS_ULCORNER);
    mvaddch(top,    right, ACS_URCORNER);
    mvaddch(bottom, left,  ACS_LLCORNER);
    mvaddch(bottom, right, ACS_LRCORNER);

    /* Extra part */
    mvhline(bottom - 2, left,  0, horiz); /* Bottom */
    mvaddch(bottom - 2, left,  ACS_LTEE);
    mvaddch(bottom - 2, right, ACS_RTEE);
}/*}}}*/

void
draw_clear(void)
{/*{{{*/
    erase();
}/*}}}*/

void
draw_end(void)
{/*{{{*/
    curs_set(1);
    endwin();
}/*}}}*/

void
draw_field(struct game_field *gf)
{/*{{{*/
    int i,
        j,
        width  = gf->width,
        height = gf->height,
        *map   = gf->map;

    for(i = 0; i < height; i++){
        for(j = 0; j < width; j++){
            mvprintw(PADDING_Y + i, PADDING_X + j * 2, "%c ", map[j + i * width] ? '*' : ' ');
        }
    }
    draw_border(gf);
}/*}}}*/

void
draw_init(void)
{/*{{{*/
    initscr();
    noecho();
    curs_set(0);
}/*}}}*/

void
draw_refresh(void)
{/*{{{*/
    refresh();
}/*}}}*/

void
draw_score(struct game *g)
{/*{{{*/
    mvprintw(PADDING_Y + 1 + g->field->height, PADDING_X, "Score: %u", g->score);
}/*}}}*/

void
draw_snake(struct snake *s)
{/*{{{*/
    struct snake_piece *sp = s->first;

    while(sp->next){
        if(sp->x != -2 && sp->y != -2){
            mvprintw(PADDING_Y + sp->y, PADDING_X + sp->x * 2, "o ");
        }
        sp = sp->next;
    }
    if(sp->x != -2 && sp->y != -2){
        mvprintw(PADDING_Y + sp->y, PADDING_X + sp->x * 2, "O ");
    }
}/*}}}*/
