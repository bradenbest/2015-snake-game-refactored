struct snake;
struct snake_piece;
struct game_field;

enum direction {
    dir_up,
    dir_right,
    dir_down,
    dir_left,
};

struct snake_piece {
    int                 x;
    int                 y;
    struct snake_piece *next;
};

struct snake {
    int                 x;
    int                 y;
    enum direction      dir;
    struct game        *game;
    struct snake_piece *first;
};

void snake_destroy(struct snake *s); 
struct snake *snake_new(int x, int y, int snake_size); 
void snake_update(struct snake *s);
