#define _BSD_SOURCE
#include <stdlib.h>
#include <unistd.h>

#include "draw.h"
#include "game.h"
#include "snake.h"

static struct game_field *game_field_new(int width, int height);
static void game_update(struct game *g);

void
game_destroy(struct game *g)
{/*{{{*/
    free(g->field->map);
    free(g->field);
    free(g);
}/*}}}*/

void
game_field_clear(struct game_field *gf)
{/*{{{*/
    int *map = gf->map,
        i;

    for(i = 0; i < gf->width * gf->height; i++){
        map[i] = 0;
    }
}/*}}}*/

void
game_field_insert_food(struct game_field *gf)
{/*{{{*/
    gf->map[rand() % (gf->width * gf->height)] = 1;
}/*}}}*/

static struct game_field *
game_field_new(int width, int height)
{/*{{{*/
    struct game_field *gf = calloc(sizeof(struct game_field), 1);
    int *map = calloc(sizeof(int), width * height);

    gf->width = width;
    gf->height = height;
    gf->map = map;

    return gf;
}/*}}}*/

struct game *
game_new(int width, int height)
{/*{{{*/
    struct game *g = calloc(sizeof(struct game), 1);
    struct game_field *gf = game_field_new(width, height);

    g->field = gf;
    return g;
}/*}}}*/

void
game_start(struct game *g)
{/*{{{*/
    g->playing = 1;
    game_field_insert_food(g->field);

    while(g->playing){
        usleep(1000000 / FPS);
        draw_clear();
        game_update(g);
        snake_update(g->snake);
        draw_refresh();
    }
}/*}}}*/

static void
game_update(struct game *g)
{/*{{{*/
    g->clock++;
    if(g->clock % 1000 == 0){
        game_field_clear(g->field);
        game_field_insert_food(g->field);
    }
    draw_field(g->field);
    draw_score(g);
}/*}}}*/
