#include <ncurses.h>

#include "input.h"

int
get_input(void)
{/*{{{*/
    int ch;
    
    timeout(0);
    ch = getch();
    return ch;
}/*}}}*/
