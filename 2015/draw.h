#define WIDTH COLS
#define HEIGHT LINES
#define PADDING_X 4
#define PADDING_Y 2

struct game;
struct game_field;
struct snake;

void draw_clear(void);
void draw_end(void);
void draw_field(struct game_field *gf);
void draw_init(void);
void draw_refresh(void);
void draw_score(struct game *g);
void draw_snake(struct snake *s);
