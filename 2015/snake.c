#include <stdlib.h>

#include "game.h"
#include "snake.h"
#include "draw.h"
#include "input.h"

static void snake_check_collision(struct snake *s);
static void snake_do_input(struct snake *s);
static void snake_move(struct snake *s);
static struct snake_piece *snake_piece_new();
static struct snake_piece *snake_popl(struct snake *s);
static void snake_push(struct snake *s, struct snake_piece *sp);
static void snake_pushl(struct snake *s, struct snake_piece *sp);

static void
snake_check_collision(struct snake *s)
{/*{{{*/
    struct snake_piece *sp = s->first;
    int i;

    while(sp->next){
        if(s->x == sp->x && s->y == sp->y){
            if(DAMAGE_MODE == REDUCE_SCORE){
                s->game->score -= 10;
                if(s->game->score < 0){
                    s->game->playing = 0;
                }
            } else if(DAMAGE_MODE == KILL_PLAYER){
                s->game->playing = 0;
            }
        }
        sp = sp->next;
    }

    if(s->game->field->map[s->x + s->y * s->game->field->width]){
        /* A little backwards: if the node at snake's (x,y) happens to be non-zero */
        game_field_clear(s->game->field);
        game_field_insert_food(s->game->field);
        for(i = 0; i < 5; i++){
            snake_pushl(s, snake_piece_new());
        }
        s->game->score += 10;
    }
}/*}}}*/

void
snake_destroy(struct snake *s)
{/*{{{*/
    struct snake_piece *sp  = s->first,
                       *sp2 = sp;

    while(sp){
        sp2 = sp->next;
        free(sp);
        sp = sp2;
    }
    free(s);
}/*}}}*/

static void
snake_do_input(struct snake *s)
{/*{{{*/
    int ch = get_input();

    switch(ch){
        case 'q':
            s->game->playing = 0;
            break;
        case 'h':
        case 'a':
            if(s->dir != dir_right){
                s->dir = dir_left;
            }
            break;
        case 'j':
        case 's':
            if(s->dir != dir_up){
                s->dir = dir_down;
            }
            break;
        case 'k':
        case 'w':
            if(s->dir != dir_down){
                s->dir = dir_up;
            }
            break;
        case 'l':
        case 'd':
            if(s->dir != dir_left){
                s->dir = dir_right;
            }
            break;
        default:
            break;
    }
}/*}}}*/

static void
snake_move(struct snake *s)
{/*{{{*/
    int width  = s->game->field->width,
        height = s->game->field->height;

    switch(s->dir){
        case dir_up:
            s->y--;
            if(s->y == -1){
                s->y = height - 1;
            }
            break;
        case dir_right:
            s->x++;
            if(s->x > width - 1){
                s->x = 0;
            }
            break;
        case dir_down:
            s->y++;
            if(s->y > height - 1){
                s->y = 0;
            }
            break;
        case dir_left:
            s->x--;
            if(s->x == -1){
                s->x = width - 1;
            }
            break;
    }
}/*}}}*/

struct snake *
snake_new(int x, int y, int snake_size)
{/*{{{*/
    struct snake *s = calloc(sizeof(struct snake), 1);
    struct snake_piece *first = snake_piece_new();
    int i;

    s->first = first;
    for(i = 0; i < snake_size; i++){
        snake_push(s, snake_piece_new());
    }
    s->x = x;
    s->y = y;

    return s;
}/*}}}*/

static struct snake_piece *
snake_piece_new(void)
{/*{{{*/
    struct snake_piece *sp = calloc(sizeof(struct snake_piece), 1);

    sp->x = -2;
    sp->y = -2;

    return sp;
}/*}}}*/

static struct snake_piece *
snake_popl(struct snake *s)
{/*{{{*/
    struct snake_piece *ret = s->first;

    s->first = s->first->next;
    ret->next = 0;

    return ret;
}/*}}}*/

static void
snake_push(struct snake *s, struct snake_piece *sp)
{/*{{{*/
    struct snake_piece *spp = s->first;

    while(spp->next){
        spp = spp->next;
    }
    spp->next = sp;
}/*}}}*/

static void
snake_pushl(struct snake *s, struct snake_piece *sp)
{/*{{{*/
    sp->next = s->first;
    s->first = sp;
}/*}}}*/

void
snake_update(struct snake *s)
{/*{{{*/
    struct snake_piece *sp;

    snake_do_input(s);
    snake_move(s);
    sp = snake_popl(s);
    sp->x = s->x;
    sp->y = s->y;
    snake_push(s, sp);
    snake_check_collision(s);
    draw_snake(s);
}/*}}}*/
