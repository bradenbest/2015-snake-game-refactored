#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "game.h"
#include "snake.h"
#include "draw.h"

static void
cleanup(struct game *g, struct snake *s)
{/*{{{*/
    game_destroy(g);
    snake_destroy(s);
    draw_end();
}/*}}}*/

int
main(void)
{/*{{{*/
    int width      = 30,
        height     = 30,
        snake_size = 10,
        score_temp;
    struct game *g;
    struct snake *s;

    srand(time(0));
    g = game_new(width, height);
    s = snake_new(width / 2, height / 2, snake_size);
    g->snake = s;
    s->game = g;
    draw_init();
    game_start(g);
    score_temp = g->score;
    cleanup(g, s);
    printf("Game Over.\n");
    if(DAMAGE_MODE == KILL_PLAYER)
        printf("Score: %i\n", score_temp);

    return 0;
}/*}}}*/
