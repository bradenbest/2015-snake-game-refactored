#define FPS 10
#define REDUCE_SCORE 1
#define KILL_PLAYER 2
#define DAMAGE_MODE KILL_PLAYER

struct game_field {
    int width;
    int height;
    int *map;
};

struct game {
    int                score;
    int                playing;
    int                clock;
    struct game_field *field;
    struct snake      *snake;
};

void game_destroy(struct game *g);
void game_field_clear(struct game_field *gf);
void game_field_insert_food(struct game_field *gf);
struct game *game_new(int width, int height);
void game_start(struct game *g);
