# 2015 Snake Game

An ncurses-based Snake Game I wrote in 2015 called "linked list snake".

I was looking through old code and came across this codebase. Some things about it struck me:

* Code style practices I use to this day are exhibited here, which is way earlier than I remember adopting them.

* The code is really bad, full of god objects, tight coupling and nonsensical design decisions.

So I set out to refactor it to be closer to the standards I have today.

## Contents

The `2015` directory has the original code from 2015.

The `2024` directory has the refactored code from 2024.

## Changes

Aside from refactoring the code, I also optimized it.

The drawing code for example cleared the whole screen and redrew everything on every tick. I optimized it by drawing the
borders only once and then clearing only inside the sub-window when it needs to be cleared. It's still per-tick but a
much smaller area. The status bar lines are only cleared when updated.

The game loop used the usleep function in a while loop to control the framerate. I optimized this by instead abstracting
the gameloop into a function that performs one step of the gameloop. From there, clock() timestamps are used to
determine when it's time to run a tick, as would be expected.

The "food" was written to a gameboard the size of the play area in the main subwindow. I optimized this by getting rid
of the entire array and just using a single int for the food. Food is drawn where it needs to be drawn instead of going
over every cell and drawing a food or a blank when there is only ever going to be one food on the play area.

The snake was implemented as a singly linked list without a tail reference, so tail insertion was O(n) and it was all
around not efficient for speed or memory. I optimized this by changing it to a circular queue. Nothing is moved; the
offset into the array moves in a circle about the array. It's really that simple.

I also removed a ton of coupling. Modules now interface with each other through regular function calls without needing
to know the implementation details of each other.

Removed a bunch of unnecessary structs as well.

## Other

Annoyingly, after all this, the game still flickers like crazy. It runs at the intended tickrate and is perfectly
responsive, but ncurses or the terminal can't keep up. I think the takeaway here is that ncurses was never designed for
running a game loopa. It's more for making tools and having an event-driven UI loop.
